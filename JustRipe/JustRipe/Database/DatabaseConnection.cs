﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using JustRipe.Models;

namespace JustRipe.Database
{
    class DatabaseConnection
    {
        private static string connectionStr;
        private static DatabaseConnection databaseConnection;


        //the SqlConnection object used to store the connection to the database
        System.Data.SqlClient.SqlConnection connectionToDB;

        // the DataAdapter object used to open a table of the database
        private System.Data.SqlClient.SqlDataAdapter dataAdapter;

        #region _instance
        /// <summary>
        /// Method for getting the only instance of this class
        /// </summary>
        /// <returns></returns>
        public static DatabaseConnection _instance()
        {
            if (databaseConnection == null)
            {
                databaseConnection = new DatabaseConnection();

                // get the connectionString from the Settings
                connectionStr = Properties.Settings.Default.ConnectionDatabase;
            }

            return databaseConnection;

        }
        #endregion

        #region OpenConnection
        /// <summary>
        /// Method for opening the connection to database
        /// </summary>
        public void OpenConnection()
        {
            // create the connection to the database as an instance of System.Data.SqlClient.SqlConnection
            connectionToDB = new
                System.Data.SqlClient.SqlConnection(connectionStr);

            //open the connection
            connectionToDB.Open();
        }
        #endregion

        #region CloseConnection
        /// <summary>
        /// Method for closing the connection to database
        /// </summary>
        public void CloseConnection()
        {
            //close the connection to the database
            connectionToDB.Close();
        }
        #endregion

        #region GetDataSet
        /// <summary>
        /// Method for getting the DataSet from the Database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public DataSet GetDataSet(string sqlStatement)
        {
            DataSet dataSet;

            //opening the connection
            OpenConnection();

            // create the object dataAdapterto manipulate a table from the database Database1 specified by connectionToDB
            dataAdapter = new System.Data.SqlClient.SqlDataAdapter(sqlStatement, connectionToDB);

            // create the dataset
            dataSet = new System.Data.DataSet();
            dataAdapter.Fill(dataSet);

            //closing the connection
            CloseConnection();

            //return the dataSet
            return dataSet;
        }
        #endregion

        #region GetDataSetAsString
        /// <summary>
        /// Method for getting the result of query as string
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public string GetDataSetAsString(string sqlStatement)
        {
            string result = "";
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            if (table.Rows.Count == 0)
                return result;


            result = table.Rows[0].ItemArray[0].ToString();


            return result;
        }
        #endregion

        #region GetDataSetAsStringList
        /// <summary>
        /// Method for getting result of SQL statement as List of strings
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<string> GetDataSetAsStringList(string sqlStatement)
        {
            List<string> result = new List<string>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Columns.Count == 1)
                    result.Add(table.Rows[i].ItemArray[0].ToString());
                else if (table.Columns.Count == 2)
                    result.Add(string.Concat(table.Rows[i].ItemArray[0].ToString(), ", ", table.Rows[i].ItemArray[1].ToString()));
            }

            return result;
        }
        #endregion

        #region CommandToDatabase
        /// <summary>
        /// Method for executing SQL statements without any output
        /// Mainly used for UPDATE, INSERT and DELETE statements
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        public void CommandToDatabase(string sqlStatement)
        {
            //Creating SQL connection
            System.Data.SqlClient.SqlConnection sqlConnection = new
                System.Data.SqlClient.SqlConnection(connectionStr);

            //Creating command for SQL
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

            //Defining command type and assigning SQL statement to this command
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sqlStatement;
            cmd.Connection = sqlConnection;

            //Opening connection, executing the command and closing the connection with database
            sqlConnection.Open();
            cmd.ExecuteReader();
            sqlConnection.Close();
        }
        #endregion

        #region GetUserDataSetAsList
        /// <summary>
        /// Method for getting the List of Users from the database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<User> GetUserDataSetAsList(string sqlStatement)
        {
            List<User> userList = new List<User>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                userList.Add
                    (
                    new User()
                    {
                        ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                        FirstName = table.Rows[i].ItemArray[1].ToString(),
                        LastName = table.Rows[i].ItemArray[2].ToString(),
                        Role = table.Rows[i].ItemArray[3].ToString(),
                        Email = table.Rows[i].ItemArray[4].ToString(),
                        Phone = table.Rows[i].ItemArray[5].ToString(),
                    }
                    );
            }
            return userList;
        }
        #endregion

        #region GetVehicleDataSetAsList
        /// <summary>
        /// Method for getting the List of Vehicles from the database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<Vehicle> GetVehicleDataSetAsList(string sqlStatement)
        {
            List<Vehicle> vehicleList = new List<Vehicle>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];


            for (int i = 0; i < table.Rows.Count; i++)
            {
                vehicleList.Add
                    (
                    new Vehicle()
                    {
                        ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                        Name = table.Rows[i].ItemArray[1].ToString(),
                        Type = table.Rows[i].ItemArray[2].ToString(),
                    }
                    );
            }
            return vehicleList;
        }
        #endregion

        #region GetContainerDataSetAsList
        /// <summary>
        /// Method for getting the List of Containers from the database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<Container> GetContainerDataSetAsList(string sqlStatement)
        {
            List<Container> containerList = new List<Container>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                containerList.Add
                    (
                    new Container()
                    {
                        ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                        Type = table.Rows[i].ItemArray[1].ToString(),
                        Capacity = Convert.ToInt32(table.Rows[i].ItemArray[2].ToString()),
                        UnitType = table.Rows[i].ItemArray[3].ToString(),
                        State = table.Rows[i].ItemArray[4].ToString(),
                        Amount = Convert.ToInt32(table.Rows[i].ItemArray[5])
                    }
                    );
            }
            return containerList;
        }
        #endregion

        #region GetCropDataSetAsList
        /// <summary>
        /// Method for getting the List of Crops from the database
        ///            3 types of Lists can be returned
        /// 1 - Crops linked with Container_Types
        /// 2 - Crops without links
        /// 3 - Case when some fields are null and C# does not accept them 
        /// It could be done other way, but group was short in time
        /// So, decision was made to keep this way
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<Crop> GetCropDataSetAsList(string sqlStatement)
        {
            List<Crop> cropList = new List<Crop>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                //Check if 13 columns returned (i.e. Crop is linked to Container_Types table)
                if (table.Columns.Count == 13 && table.Rows[i].ItemArray[2].ToString() != "requested" && table.Rows[i].ItemArray[2].ToString() != "seeds")
                {
                    cropList.Add
                        (
                        new Crop()
                        {
                            ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                            Name = table.Rows[i].ItemArray[1].ToString(),
                            State = table.Rows[i].ItemArray[2].ToString(),
                            Type = table.Rows[i].ItemArray[3].ToString(),
                            SowingMethod = table.Rows[i].ItemArray[4].ToString(),
                            TreatmentType = table.Rows[i].ItemArray[5].ToString(),
                            HarvestMethod = table.Rows[i].ItemArray[6].ToString(),
                            MinTemperature = Convert.ToInt32(table.Rows[i].ItemArray[7].ToString()),
                            MaxTemperature = Convert.ToInt32(table.Rows[i].ItemArray[8].ToString()),
                            ContainerType = table.Rows[i].ItemArray[9].ToString(),
                            Capacity = Convert.ToInt32(table.Rows[i].ItemArray[10].ToString()),
                            UnitType = table.Rows[i].ItemArray[11].ToString(),
                            NumOfContainers = Convert.ToInt32(table.Rows[i].ItemArray[12].ToString()),
                        }
                        );
                }
                //Crop is not linked, so fields related to container will not be assigned
                else if (table.Columns.Count == 11)
                {
                    cropList.Add
                       (
                       new Crop()
                       {
                           ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                           Name = table.Rows[i].ItemArray[1].ToString(),
                           State = table.Rows[i].ItemArray[2].ToString(),
                           Type = table.Rows[i].ItemArray[3].ToString(),
                           SowingMethod = table.Rows[i].ItemArray[4].ToString(),
                           TreatmentType = table.Rows[i].ItemArray[5].ToString(),
                           HarvestMethod = table.Rows[i].ItemArray[6].ToString(),
                           MinTemperature = Convert.ToInt32(table.Rows[i].ItemArray[7].ToString()),
                           MaxTemperature = Convert.ToInt32(table.Rows[i].ItemArray[8].ToString()),
                           NumOfContainers = Convert.ToInt32(table.Rows[i].ItemArray[10].ToString()),
                       }
                       );
                }
                // When crop is in a state requested or seeds and does not have any methods for sowing, harvesting, etc.
                else
                {
                    cropList.Add
                            (
                            new Crop()
                            {
                                ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                                Name = table.Rows[i].ItemArray[1].ToString(),
                                State = table.Rows[i].ItemArray[2].ToString(),
                                Type = table.Rows[i].ItemArray[3].ToString(),
                                ContainerType = table.Rows[i].ItemArray[9].ToString(),
                                Capacity = Convert.ToInt32(table.Rows[i].ItemArray[10].ToString()),
                                UnitType = table.Rows[i].ItemArray[11].ToString(),
                                NumOfContainers = Convert.ToInt32(table.Rows[i].ItemArray[12].ToString()),
                            }
                            );
                }
            }
            return cropList;
        }
        #endregion

        #region GetFertiliserDataSetAsList
        /// <summary>
        /// Method for getting the List of Fertilisers from the database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<Fertiliser> GetFertiliserDataSetAsList(string sqlStatement)
        {
            List<Fertiliser> fertiliserList = new List<Fertiliser>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                fertiliserList.Add
                    (
                    new Fertiliser()
                    {
                        ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                        Type = table.Rows[i].ItemArray[1].ToString(),
                        State = table.Rows[i].ItemArray[2].ToString(),
                        Amount = Convert.ToInt32(table.Rows[i].ItemArray[3].ToString()),
                    }
                    );
            }
            return fertiliserList;
        }
        #endregion

        #region GetFieldDataSetAsList
        /// <summary>
        /// Method for getting the List of Fields from the database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<Field> GetFieldDataSetAsList(string sqlStatement)
        {
            List<Field> fieldList = new List<Field>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Columns.Count == 6)
                {
                    fieldList.Add
                        (
                        new Field()
                        {
                            ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                            CropName = table.Rows[i].ItemArray[1].ToString(),
                            Type = table.Rows[i].ItemArray[2].ToString(),
                            SowingMethod = table.Rows[i].ItemArray[3].ToString(),
                            TreatmentType = table.Rows[i].ItemArray[4].ToString(),
                            HarvestMethod = table.Rows[i].ItemArray[5].ToString(),
                        }
                        );
                }
                else
                {
                    fieldList.Add
                        (
                         new Field()
                         {
                             ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                             CropName = "NULL",
                         }
                        );
                }
            }
            return fieldList;
        }
        #endregion

        #region GetAttendancesDataSetAsList
        /// <summary>
        /// Method for getting the List of CheckIns from the database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<Attendance> GetAttendancesDataSetAsList(string sqlStatement)
        {
            List<Attendance> attendances = new List<Attendance>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                attendances.Add
                    (
                    new Attendance()
                    {
                        ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                        UserID = Convert.ToInt32(table.Rows[i].ItemArray[1].ToString()),
                        Date = Convert.ToDateTime(table.Rows[i].ItemArray[2]).ToLongDateString(),
                    }
                    );
            }
            return attendances;
        }
        #endregion

        #region GetLabourDataSetAsList
        /// <summary>
        /// Method for getting the List of Labours from the database
        /// </summary>
        /// <param name="sqlStatement"></param> SQL command
        /// <returns></returns>
        public List<Labour> GetLabourDataSetAsList(string sqlStatement)
        {
            List<Labour> labourList = new List<Labour>();
            DataSet dataSet = GetDataSet(sqlStatement);
            DataTable table = dataSet.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Columns.Count == 10)
                {
                    labourList.Add
                        (
                        new Labour()
                        {
                            ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                            LabourerName = string.Concat(table.Rows[i].ItemArray[1].ToString(), " ", table.Rows[i].ItemArray[2].ToString()),
                            VehicleName = string.Concat(table.Rows[i].ItemArray[3].ToString(), ", ", table.Rows[i].ItemArray[4].ToString()),
                            CropName = table.Rows[i].ItemArray[5].ToString(),
                            FertiliserType = table.Rows[i].ItemArray[6].ToString(),
                            LabourType = table.Rows[i].ItemArray[7].ToString(),
                            NumbOfLabourers = Convert.ToInt32(table.Rows[i].ItemArray[8].ToString()),
                            LabourDate = Convert.ToDateTime(table.Rows[i].ItemArray[9]).ToLongDateString(),
                        }
                        );
                }
                else
                {
                    labourList.Add
                        (
                        new Labour()
                        {
                            ID = Convert.ToInt32(table.Rows[i].ItemArray[0].ToString()),
                            LabourerName = string.Concat(table.Rows[i].ItemArray[1].ToString(), " ", table.Rows[i].ItemArray[2].ToString()),
                            CropName = table.Rows[i].ItemArray[3].ToString(),
                            FertiliserType = table.Rows[i].ItemArray[4].ToString(),
                            LabourType = table.Rows[i].ItemArray[5].ToString(),
                            NumbOfLabourers = Convert.ToInt32(table.Rows[i].ItemArray[6].ToString()),
                            LabourDate = Convert.ToDateTime(table.Rows[i].ItemArray[7]).ToLongDateString(),
                        }
                        );
                }
            }
            return labourList;
        }
        #endregion
    }
}
