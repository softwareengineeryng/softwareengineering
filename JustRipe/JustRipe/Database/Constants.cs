﻿using System;

namespace JustRipe.Database
{
    class Constants
    {
        //SQL statements 
        private static String selectFrom = "SELECT {0} FROM [{1}]"; //Square brackets for preventing the bug, because SQL has key word User
        private static String selectFromOrderBy = "SELECT {0} FROM [{1}] ORDER BY {2}";
		private static String selectFromWhere = "SELECT {0} FROM [{1}] WHERE {2}";
        private static String selectFromJoin = "SELECT {0} FROM [{1}] JOIN [{2}] ON {3}";
        private static String selectFromJoinWhere = "SELECT {0} FROM [{1}] JOIN [{2}] ON {3} WHERE {4}";
        private static String selectFromJoinX2 = "SELECT {0} FROM [{1}] JOIN [{2}] ON {3} JOIN [{4}] ON {5}";
        private static String selectFromJoinX3 = "SELECT {0} FROM [{1}] JOIN [{2}] ON {3} JOIN [{4}] ON {5} JOIN [{6}] ON {7}";
        private static String selectFromJoinX4 = "SELECT {0} FROM [{1}] JOIN [{2}] ON {3} JOIN [{4}] ON {5} JOIN [{6}] ON {7} JOIN [{8}] ON {9}";

        private static String insertToUser = "INSERT INTO [User] (firstName, lastName, email, phone, username, password) VALUES ('{0}', '{1}', '{2}','{3}', '{4}', '{5}')";
        private static String insertToUserRole = "INSERT INTO [User-Role] (userID, roleID) VALUES ({0} , '{1}')";
        private static String insertToAttendances = "INSERT INTO [Attendances] (userID, date) VALUES ({0} , N'{1}')";
        private static String insertToVehicle = "INSERT INTO [Vehicle] (name, type) VALUES ('{0}', '{1}')";
        private static String insertToContainer = "INSERT INTO [Container] (container_TypeID, state, amount) VALUES ({0}, '{1}', {2})";
        private static String insertToCrop = "INSERT INTO [Crop] (name, state, type, sowingMethod, treatmentType, harvestMethod, minTemperature, maxTemperature, numOfContainers) VALUES ('{0}', '{1}', '{2}','{3}', '{4}', '{5}', {6}, {7}, {8})";
        private static String insertToFertiliser = "INSERT INTO [Fertiliser] (type, state, amount) VALUES ('{0}', '{1}', {2})";
        private static String insertToField = "INSERT INTO [Field] (cropID) VALUES ({0})";
        private static String insertToLabour = "INSERT INTO [Labour] (vehicleID, cropID, labourType, numberOfLabourersNeeded, labourDate) VALUES ({0},{1},'{2}',{3},'{4}')";
        private static String insertToUsersLabours = "INSERT INTO [Users-Labours] (userID, labourID) VALUES ({0},{1})";

        private static String updateSetWhere = "UPDATE [{0}] SET {1} WHERE {2}";
        
        private static String deleteFromWhere = "DELETE FROM [{0}] WHERE {1}";


        #region SelectFrom
        /// <summary>
        /// Method for Selecting field(s) from table
        /// </summary>
        /// <param name="field"></param> can be null, otherwise method crushes
        /// <param name="table"></param> cannot be null, method crushes
        /// <returns></returns>
        public static string SelectFrom(string field, string table)
		{
			//Checking if fields is NullOrEmpty
			//If true = field will be equal to "*"
			//Else field will stay the same
			field = string.IsNullOrEmpty(field) ? "*" : field;

			//returning the selectFrom string with field and table inserted to it
			return string.Format(selectFrom, field, table);
		}
        #endregion

        #region SelectFromOrderBy
        /// <summary>
        /// Method for Selecting field(s) from table and ordering by field
        /// </summary>
        /// <param name="field"></param> can be null, otherwise method crushes
        /// <param name="table"></param> cannot be null, method crushes
        /// <param name="orderBy"></param> field to be ordered by
        /// <returns></returns>
        public static string SelectFromOrderBy(string field, string table, string orderBy)
        {
            //Checking if fields is NullOrEmpty
            //If true = field will be equal to "*"
            //Else field will stay the same
            field = string.IsNullOrEmpty(field) ? "*" : field;

            //returning the selectFrom string with field and table inserted to it
            return string.Format(selectFromOrderBy, field, table, orderBy);
        }
        #endregion

        #region SelectFromWhere
        /// <summary>
        /// Method for getting results from using WHERE in SQL statement
        /// </summary>
        /// <param name="field"></param> field in that particular table
        /// <param name="table"></param> table in the database
        /// <param name="condition"></param> condition for WHERE
        /// <returns></returns>
        public static string SelectFromWhere(string field, string table, string condition)
        {
            //Checking if fields is NullOrEmpty
            //If true = field will be equal to "*"
            //Else field will stay the same
            field = string.IsNullOrEmpty(field) ? "*" : field;

            return string.Format(selectFromWhere,field,table,condition);
        }
        #endregion

        #region SelectFromJoin
        /// <summary>
        /// MEthod for getting SQL command Select From Join On using given variables
        /// </summary>
        /// <param name="fields"></param> fields needed
        /// <param name="table1"></param> table name 1
        /// <param name="table2"></param> table name 2
        /// <param name="onCondition"></param> condition for ON
        /// <returns></returns>
        public static string SelectFromJoin(string fields, string table1, string table2, string onCondition)
        {
            return string.Format(selectFromJoin, fields, table1, table2, onCondition);
        }
        #endregion

        #region SelectFromJoinX2
        /// <summary>
        /// MEthod for getting SQL command Select From Join On using given variables
        /// </summary>
        /// <param name="fields"></param> fields needed
        /// <param name="table1"></param> table name 1
        /// <param name="table2"></param> table name 2
        /// <param name="table3"></param> table name 3
        /// <param name="onCondition1"></param> condition for ON
        /// <param name="onCondition2"></param> condition for ON
        /// <returns></returns>
        public static string SelectFromJoinX2(string fields, string table1, string table2, string onCondition1, string table3, string onCondition2)
        {
            return string.Format(selectFromJoinX2, fields, table1, table2, onCondition1,table3,onCondition2);
        }
        #endregion

        #region SelectFromJoinX3
        /// <summary>
        /// MEthod for getting SQL command Select From Join On using given variables
        /// </summary>
        /// <param name="fields"></param> fields needed
        /// <param name="table1"></param> table name 1
        /// <param name="table2"></param> table name 2
        /// <param name="table3"></param> table name 3
        /// <param name="table4"></param> table name 4
        /// <param name="onCondition1"></param> condition for ON
        /// <param name="onCondition2"></param> condition for ON
        /// <param name="onCondition3"></param> condition for ON
        /// <returns></returns>
        public static string SelectFromJoinX3(string fields, string table1, string table2, string onCondition1, string table3, string onCondition2, string table4, string onCondition3)
        {
            return string.Format(selectFromJoinX3, fields, table1, table2, onCondition1, table3, onCondition2, table4, onCondition3);
        }
        #endregion

        #region SelectFromJoinX4
        /// <summary>
        /// MEthod for getting SQL command Select From Join On using given variables
        /// </summary>
        /// <param name="fields"></param> fields needed
        /// <param name="table1"></param> table name 1
        /// <param name="table2"></param> table name 2
        /// <param name="table3"></param> table name 3
        /// <param name="table4"></param> table name 4
        /// <param name="table5"></param> table name 5
        /// <param name="onCondition1"></param> condition for ON
        /// <param name="onCondition2"></param> condition for ON
        /// <param name="onCondition3"></param> condition for ON
        /// <param name="onCondition4"></param> condition for ON
        /// <returns></returns>
        public static string SelectFromJoinX4(string fields, string table1, string table2, string onCondition1,
            string table3, string onCondition2, string table4, string onCondition3, string table5, string onCondition4)
        {
            return string.Format(selectFromJoinX4, fields, table1, table2,onCondition1,
                                 table3, onCondition2, table4, onCondition3, table5, onCondition4);
        }
        #endregion

        #region SelectFromJoinWhere
        /// <summary>
        /// 
        /// MEthod for getting SQL command Select From Join On Where using given variables
        /// </summary>
        /// <param name="field"></param> fields needed
        /// <param name="table1"></param> table name 1
        /// <param name="table2"></param> table name 2
        /// <param name="onCondition"></param> condition for ON
        /// <param name="condition"></param> condition for WHERE
        /// <returns></returns>
        public static string SelectFromJoinWhere(string field, string table1, string table2, string onCondition, string condition)
        {
            return string.Format(selectFromJoinWhere, field, table1, table2, onCondition,condition);
        }
        #endregion

        #region InsertToVehicle
        /// <summary>
        /// Method for inserting new vehicle to the database
        /// </summary>
        /// <param name="vehicleName"></param>
        /// <param name="vehicleType"></param>
        /// <returns></returns>
        public static string InsertToVehicle(string vehicleName, string vehicleType)
        {
            string str = string.Format(insertToVehicle, vehicleName, vehicleType);
           
            return string.Concat(str);

        }
        #endregion

        #region InsertToUser
        /// <summary>
        /// Method for inserting new user to the databse
        /// And assigning them to a role(Manager/Labourer)
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="roleID"></param> 1 - Manager; 2 - Labourer
        /// <returns></returns>
        public static string InsertToUser(string firstName, string lastname, string email, string phone, string username, string password, int roleID)
        {
            string str1 = string.Format(insertToUser, firstName, lastname, email, phone, username, password);
            string str2 = string.Format(insertToUserRole, "@@Identity", roleID);

            return string.Concat(str1, str2);
        }
        #endregion

        #region InsertToAttendances
        /// <summary>
        /// Method for Checking-In the user's attendance when they log-in
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string InsertToAttendances (int userID, string date)
        {
            return string.Format(insertToAttendances, userID, date);
        }
        #endregion

        #region InsertToContainer
        /// <summary>
        /// Method inserting new container to the database
        /// </summary>
        /// <param name="cropID"></param>
        /// <param name="weight"></param>
        /// <param name="size"></param>
        /// <param name="available"></param>
        /// <returns></returns>
        public static string InsertToContainer(int cropID, int weight, float size, bool available)
        {
            return string.Format(insertToContainer, cropID, weight, size, available);
        }
        #endregion

        #region InsertToCrop
        /// <summary>
        /// Method for inserting new crop to the database
        /// </summary>
        /// <param name="name"></param>
        /// <param name="state"></param>
        /// <param name="type"></param>
        /// <param name="sowingMethod"></param>
        /// <param name="treatmentType"></param>
        /// <param name="harvestMethod"></param>
        /// <param name="minTemperature"></param>
        /// <param name="maxTemperature"></param>
        /// <returns></returns>
        public static string InsertToCrop(string name, string state, string type, string sowingMethod, string treatmentType, string harvestMethod, float minTemperature, float maxTemperature, int numOfContainers)
        {
            return string.Format(insertToCrop, name, state, type, sowingMethod, treatmentType, harvestMethod, minTemperature, maxTemperature,numOfContainers);
        }
        #endregion

        #region InsertToFertiliser
        /// <summary>
        /// Method for inserting new fertiliser to the database
        /// </summary>
        /// <param name="type"></param>
        /// <param name="state"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static string InsertToFertiliser(string type, string state, float amount)
        {
            return string.Format(insertToFertiliser, type, state, amount);
        }
        #endregion

        #region InsertToField
        /// <summary>
        /// Method for inserting new field to the database
        /// </summary>
        /// <param name="cropID"></param>
        /// <returns></returns>
        public static string InsertToField(int cropID)
        {
            return string.Format(insertToField, cropID);
        }
        #endregion

        #region InsertToLabour
        /// <summary>
        /// Method for inserting new field to the database
        /// </summary>
        /// <param name="cropID"></param>
        /// <returns></returns>
        public static string InsertToLabour(int vehicleID, int cropID, string labourType, int numberOfLabourersNeeded, string labourDate, int labourerID1, int labourerID2)
        {
            string str1;
            string str2;
            string str3;

            if (vehicleID == -1)
                 str1 = string.Format(insertToLabour, "NULL", cropID, labourType, numberOfLabourersNeeded, labourDate);
            else
                str1 = string.Format(insertToLabour, vehicleID, cropID, labourType, numberOfLabourersNeeded, labourDate);

            if(numberOfLabourersNeeded == 2)
            {
                str2 = string.Format(insertToUsersLabours, labourerID1, "@@Identity");
                str3 = string.Format(insertToUsersLabours, labourerID2, "@@Identity");

                return string.Concat(str1,str2,str3);
            }
            else
            {
                str2 = string.Format(insertToUsersLabours, labourerID1, "@@Identity");
                return string.Concat(str1, str2);
            }
        }
        #endregion

        #region UpdateSetWhere
        /// <summary>
        /// Method for updating the field(s) in the table with given condition
        /// </summary>
        /// <param name="table"></param> table name in the database
        /// <param name="updatingField"></param> field to be updated and value
        /// <param name="condition"></param> condition for WHERE
        /// <returns></returns>
        public static string UpdateSetWhere(string table, string updatingField, string condition)
        {
            return string.Format(updateSetWhere, table, updatingField, condition);
        }
        #endregion

        #region DeleteFromUser
        /// <summary>
        /// Method for deleting the user from the database
        /// And deleting user's connection to role(s) in User-Role table
        /// </summary>
        /// <param name="userID"></param> unique ID
        /// <returns></returns>
        public static string DeleteFromUser(int userID)
        {
            string str1 = string.Format(deleteFromWhere, "User-Role", string.Format("userID = {0}", userID));
            string str2 = string.Format(deleteFromWhere,"Attendances", string.Format("userID = {0}", userID));
            string str3 = string.Format(deleteFromWhere,"User", string.Format("Id = {0}", userID));
            return string.Concat(str1, str2, str3);
        }
        #endregion

        #region DeleteFromvehicle
        /// <summary>
        /// Method for deleting the vehicle from the database
        /// And deleting vehicle's connection to role(s) in User-Role table
        /// </summary>
        /// <param name="vehicleID"></param> unique ID
        /// <returns></returns>
        public static string DeleteFromVehicle(int vehicleID)
        {
            string str1 = string.Format(updateSetWhere,"Labour" ,"vehicleID = NULL",string.Format("vehicleID = '{0}'", vehicleID));
            string str2 = string.Format(deleteFromWhere,"Vehicle", string.Format("Id = {0}", vehicleID));
            return string.Concat(str1, str2);
        }
        #endregion

        #region DeleteFromWhereByID
        /// <summary>
        /// Method for deleting row in table using unique ID
        /// </summary>
        /// <param name="table"></param> table name
        /// <param name="id"></param> id in the table
        /// <returns></returns>
        public static string DeleteFromWhereByID(string table, int id)
		{
			return string.Format(deleteFromWhere, table, string.Format("Id = {0}", id));
		}
        #endregion
    }
}
