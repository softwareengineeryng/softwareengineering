﻿using JustRipe.Database;
using JustRipe.Models;
using System.Collections.Generic;

namespace JustRipe.ViewModels
{
    class AttendanceViewModel
    {
        /// <summary>
        /// Method for getting the list of all attendance records from the database
        /// </summary>
        /// <returns></returns>
        public List<Attendance> GetAttendances()
        {
            return DatabaseConnection._instance().GetAttendancesDataSetAsList(Constants.SelectFromOrderBy("", "Attendances","date DESC"));
        }

        /// <summary>
        /// Method for getting the list of all attendance records from the database using the userID
        /// </summary>
        /// <param name="userID"></param> unique user ID
        /// <returns></returns>
        public List<Attendance> GetAttendancesByUserID(int userID)
        {
            return DatabaseConnection._instance().GetAttendancesDataSetAsList(Constants.SelectFromWhere("", "Attendances",string.Format("userID = {0} ORDER BY date DESC",userID)));
        }
    }
}
