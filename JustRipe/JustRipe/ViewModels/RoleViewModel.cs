﻿using System.ComponentModel;
using JustRipe.Database;

namespace JustRipe.ViewModels
{
    class RoleViewModel
    {
        public string GetRoleByUserID(int userID)
        {
            return DatabaseConnection._instance().GetDataSetAsString(string.Format("select [name] from [Role] join [User-Role] on [Role].[Id] = [User-Role].[roleID] where userID= {0}",userID));
        }
    }
}
