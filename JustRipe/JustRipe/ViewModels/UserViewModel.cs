﻿using System;
using System.Collections.Generic;
using System.Windows;
using JustRipe.Database;
using JustRipe.Models;
using JustRipe.Utility;
using JustRipe.Views.LoginAndMPs;

namespace JustRipe.ViewModels
{
    class UserViewModel
    {
        private static String userFields = "[User].[Id], [firstName], [lastName], [Role].[name], [email], [phone]";
        public static int UserID { get; private set; }
        public static string Role { get; private set; }

        /// <summary>
        /// Method for checking if user with current username exists
        /// And password matches with it
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public object CheckCredentials(string username, string password)
        {
            RoleViewModel roleVM = new RoleViewModel();

            //Catching Exception if user does not exist in the database
            try
            {
                UserID = Convert.ToInt32(DatabaseConnection._instance().GetDataSetAsString(Constants.SelectFromWhere("Id", "User", string.Format("username = '{0}'", username))));
            }
            catch
            {
                return null;
            }

            //Getting role of the user
            Role = roleVM.GetRoleByUserID(UserID);
            
            //Verifying the password with stored and encrypted password in database
            if (ComparePasswords(password))
            {
                //Check if user's role is Labourer
                if (Role == "Labourer")
                {
                    //Records user's attendance to the database
                    RecordAttendance(UserID);
                    //Returns page to be opened
                    return new LabourerMP();
                }
                //Check if user's role is Manager
                else if (Role== "Manager")
                {
                    //Records user's attendance to the database
                    RecordAttendance(UserID);
                    
                    //Returns page to be opened
                    return new ManagerMP();
                }
                //If not matches, return null
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Method for checking if input password matches with the one in the database 
        /// </summary>
        /// <param name="inputPassword"></param> Password entered by the user to check
        /// <returns></returns>
        public bool ComparePasswords(string inputPassword)
        {
            //Getting old password of ther user
            string oldPassword = DatabaseConnection._instance().GetDataSetAsString(Constants.SelectFromWhere("password", "User", string.Format("Id = {0}", UserID)));

            //Returning the result of comparing input and old password
            return Helpers.VerifyHash(inputPassword, oldPassword);
        }

        public void UpdatePassword(string newPassword)
        {
            //Encrypting the new password
            string encryptedPassword = Helpers.ComputeHash(newPassword, null);

            //Updating the password in the database
            DatabaseConnection._instance().CommandToDatabase(Constants.UpdateSetWhere("User",string.Format("password = '{0}'",encryptedPassword), string.Format("Id = {0}", UserID)));
        }

        /// <summary>
        /// Method for recording user's log-in to the Database
        /// </summary>
        /// <param name="userID"></param>
        private static void RecordAttendance(int userID)
        {
            string date = DateTime.Now.Date.ToString("MM/dd/yyyy");
            string lastRecordedDate = Convert.ToDateTime(DatabaseConnection._instance().GetDataSetAsString(Constants.SelectFromWhere("TOP 1 date", "Attendances", string.Format("[userID]= {0} ORDER BY date DESC", userID)))).ToString("MM/dd/yyyy");
           
            //In order to prevent double recording during the same date
            //Program checks if todays date is already recorded for this user
            if (!string.Equals(lastRecordedDate, date))
            {
                DatabaseConnection._instance().CommandToDatabase(Constants.InsertToAttendances(userID, date));
            }
        }

        /// <summary>
        /// Method for adding the User to the Database
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="isManager"></param>
        public void AddUserToDB(string firstName, string lastName, string email, string phone, string username, string password, bool isManager)
        {
            if (isManager)
            {
                DatabaseConnection._instance().CommandToDatabase(Constants.InsertToUser(firstName, lastName, email, phone, username, password, 1));
            }
            else
            {
                DatabaseConnection._instance().CommandToDatabase(Constants.InsertToUser(firstName, lastName, email, phone, username, password, 2));
            }
        }

        /// <summary>
        /// Method for deleting the user using userID
        /// </summary>
        /// <param name="userID"></param> unique ID
        public void DeleteUserFromDB(int userID)
        {
            DatabaseConnection._instance().CommandToDatabase(Constants.DeleteFromUser(userID));
        }

        /// <summary>
        /// Method for getting the user's ID
        /// </summary>
        /// <param name="users"></param> List of Users
        /// <param name="index"></param> Index of chosen user on DataGrid
        /// <returns></returns>
        public int GetUserID(List<User> users, int index)
        {
            if (index >= 0)
            {
                List<int> userIDs = new List<int>();

                foreach (var user in users)
                {
                    userIDs.Add(user.ID);
                }
                return userIDs[index];
            }
            return -1;
        }

        /// <summary>
        /// Method for getting the list of all users from the database
        /// </summary>
        /// <returns></returns>
        public List<User> GetUsers()
        {
            return DatabaseConnection._instance().GetUserDataSetAsList(Constants.SelectFromJoinX2(userFields,"User","User-Role","[User].[Id] = userID","Role","roleID = [Role].[Id] ORDER BY [Role].[name] DESC"));
        }

        /// <summary>
        /// Method for getting the list of all labourers from the database
        /// </summary>
        /// <returns></returns>
        public List<User> GetLabourers(int userID)
        {
            if (userID == -1)
                return DatabaseConnection._instance().GetUserDataSetAsList(Constants.SelectFromJoinX2(userFields, "User", "User-Role", "[User].[Id] = userID", "Role", "roleID = [Role].[Id] WHERE [Role].[name] = 'Labourer'"));
            else
                return DatabaseConnection._instance().GetUserDataSetAsList(Constants.SelectFromJoinX2(userFields, "User", "User-Role", "[User].[Id] = userID", "Role", string.Format("roleID = [Role].[Id] WHERE [Role].[name] = 'Labourer' and [User].[Id] != {0}", userID)));
        }
    }
}
