﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using JustRipe.Database;
using JustRipe.Models;

namespace JustRipe.ViewModels
{
    class ContainerViewModel
    {
        string containerFields = "[Container].[Id], [type], [capacity], [unit_type], [state], [amount]";

        public List<string> GetContainerTypes()
        {
            return DatabaseConnection._instance().GetDataSetAsStringList(Constants.SelectFrom("DISTINCT type", "Container_Types"));
        }

        public List<string> GetContainerCapacities(string containerType)
        {
            return DatabaseConnection._instance().GetDataSetAsStringList(Constants.SelectFromWhere("capacity, unit_type", "Container_Types", string.Format("[type] = '{0}'",containerType)));
        }

        public int GetContainerID(List<Models.Container> containers, int index)
        {
            if (index >= 0)
            {
                List<int> containerIDs = new List<int>();

                foreach (var container in containers)
                {
                    containerIDs.Add(container.ID);
                }
                return containerIDs[index];
            }
            return -1;
        }
        public void UpdateContainerInDB(string type, int capacity, string state, int amount)
        {
            int oldAmount = Convert.ToInt32(DatabaseConnection._instance().GetDataSetAsString(Constants.SelectFromJoinWhere("amount", "Container", "Container_Types", "container_TypeID = [Container_Types].[Id]", string.Format("state = '{0}' AND capacity = {1}", state, capacity))));
            int newAmount = oldAmount + amount;

            //As Container table is linked with Container_Types, for updating first ID of a Container is found
            int id = Convert.ToInt32(DatabaseConnection._instance().GetDataSetAsString(Constants.SelectFromJoinWhere("Container.Id","Container","Container_Types", "container_TypeID = [Container_Types].[Id]", string.Format("state = '{0}' AND type = '{1}' AND capacity = {2}", state, type, capacity))));

            //Update the Container using its ID
            DatabaseConnection._instance().CommandToDatabase(Constants.UpdateSetWhere("Container", string.Format("amount = {0}", newAmount), string.Format("Id = {0}",id)));
        }
        public void CompleteRequest(int id)
        {
            //Always returns single result, so index 0 will be used only
            List<Models.Container> containers = DatabaseConnection._instance().GetContainerDataSetAsList(Constants.SelectFromJoinWhere(containerFields, "Container", "Container_Types", "container_TypeID = [Container_Types].[Id]", string.Format("Container.Id = {0}", id)));

            //Update the stock with new amount of fertiliser
            UpdateContainerInDB(containers[0].Type, containers[0].Capacity, "in-stock", containers[0].Amount);

            //Update the requested fertiliser amount to 0, as it is completed
            UpdateContainerInDB(containers[0].Type, containers[0].Capacity, containers[0].State, -containers[0].Amount);
        }

        public List<Models.Container> GetContainersByState(string state)
        {
            if (state == "requested")
            {
                return DatabaseConnection._instance().GetContainerDataSetAsList(Constants.SelectFromJoinWhere(containerFields, "Container", "Container_Types", "container_TypeID = [Container_Types].[Id]", "state = 'requested' AND amount > 0"));
            }
            else if (state == "In-stock")
            {
                return DatabaseConnection._instance().GetContainerDataSetAsList(Constants.SelectFromJoinWhere(containerFields, "Container", "Container_Types", "container_TypeID = [Container_Types].[Id]", "state = 'in-stock'"));
            }
            else
            {
                return null;
            }
        }
    }
}
