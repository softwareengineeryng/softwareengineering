﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using JustRipe.Database;
using JustRipe.Models;

namespace JustRipe.ViewModels
{
    class FieldViewModel
    {
        string busyFieldColumns = "[Field].[Id], [Crop].[name], [Crop].[type], [Crop].[sowingMethod], [Crop].[treatmentType], [Crop].[harvestMethod]";
        string freeFieldColumns = "[Field].[Id], [Field].[cropID]";

        public List<Field> GetFieldsByState(string state)
        {
            if (state == "busy")
            {
                return DatabaseConnection._instance().GetFieldDataSetAsList(Constants.SelectFromJoin(busyFieldColumns, "Field", "Crop", "cropID = [Crop].[Id]"));
            }
            else if (state == "available")
            {
                return DatabaseConnection._instance().GetFieldDataSetAsList(Constants.SelectFromWhere(freeFieldColumns, "Field", "cropID IS NULL"));
            }
            else
            {
                return null;
            }
        }
    }
}
