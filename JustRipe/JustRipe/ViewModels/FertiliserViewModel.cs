﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using JustRipe.Database;
using JustRipe.Models;

namespace JustRipe.ViewModels
{
    class FertiliserViewModel
    {
        public void AddFertiliserToDB(string type, string state, int amount)
        {
            DatabaseConnection._instance().CommandToDatabase(Constants.InsertToFertiliser(type, state, amount));
        }

        public void UpdateFertiliserInDB(string type, string state, int amount)
        {
            int oldAmount = Convert.ToInt32(DatabaseConnection._instance().GetDataSetAsString(Constants.SelectFromWhere("amount","Fertiliser",string.Format("state = '{0}' AND type = '{1}'", state, type))));
            int newAmount = oldAmount + amount;

            DatabaseConnection._instance().CommandToDatabase(Constants.UpdateSetWhere("Fertiliser", string.Format("amount = {0}",newAmount), string.Format("state = '{0}' AND type = '{1}'", state, type)));
        }

        public void RequestFertiliser(string type, int amount)
        {
            string state = "requested";
            List<Fertiliser> fertilisers = DatabaseConnection._instance().GetFertiliserDataSetAsList(Constants.SelectFromWhere("", "Fertiliser", string.Format("state = '{0}' AND type = '{1}'", state, type)));

            if(fertilisers.Count == 1)
            {
                UpdateFertiliserInDB(type, state, amount);
            }
            else
            {
                AddFertiliserToDB(type, state, amount);
            }
        }

        public List<string> GetFertiliserTypes()
        {
            return DatabaseConnection._instance().GetDataSetAsStringList(Constants.SelectFrom("DISTINCT type", "Fertiliser"));
        }

        public void CompleteRequest(int id)
        {
            //Always returns single result, so index 0 will be used only
            List<Fertiliser> fertilisers = DatabaseConnection._instance().GetFertiliserDataSetAsList(Constants.SelectFromWhere("","Fertiliser",string.Format("Id = {0}",id)));

            //Update the stock with new amount of fertiliser
            UpdateFertiliserInDB(fertilisers[0].Type, "in-stock", fertilisers[0].Amount);

            //Update the requested fertiliser amount to 0, as it is completed
            UpdateFertiliserInDB(fertilisers[0].Type, fertilisers[0].State,-fertilisers[0].Amount);
        }

        /// <summary>
        /// Method for getting the fertiliser's ID
        /// </summary>
        /// <param name="fertilisers"></param> List of Fertilsers
        /// <param name="index"></param> Index of chosen user on DataGrid
        /// <returns></returns>
        public int GetFertiliserID(List<Fertiliser> fertilisers, int index)
        {
            if (index >= 0)
            {
                List<int> fertiliserIDs = new List<int>();

                foreach (var fertiliser in fertilisers)
                {
                    fertiliserIDs.Add(fertiliser.ID);
                }
                return fertiliserIDs[index];
            }
            return -1;
        }

        public List<Fertiliser> GetFertilisersByState(string state)
        {
            if (state == "Fertiliser - In stock")
            {
                return DatabaseConnection._instance().GetFertiliserDataSetAsList(Constants.SelectFromWhere("", "Fertiliser", "state = 'in-stock'"));
            }
            else if (state == "Fertiliser - Requested" || state == "requested")
            {
                return DatabaseConnection._instance().GetFertiliserDataSetAsList(Constants.SelectFromWhere("", "Fertiliser", "state = 'requested' AND amount > 0"));
            }
            else
            {
                return null;
            }
        }
    }
}
