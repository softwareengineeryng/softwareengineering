﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using JustRipe.Models;
using JustRipe.Database;
using System.Windows;

namespace JustRipe.ViewModels
{
    class CropViewModel
    {
        private static String cropFields = "[Crop].[Id], [name], [state], [Crop].[type], [sowingMethod], [treatmentType], [harvestMethod], [minTemperature], [maxTemperature], [Container_Types].[type], [capacity], [unit_type], [numOfContainers]";

		public void UpdateCropDetails(int cropID, int minTemperature, int maxTemperature, string sowingMethod, string treatmentType, string harvestMethod)
		{
			DatabaseConnection._instance().CommandToDatabase(Constants.UpdateSetWhere("Crop",string.Format("minTemperature = {0}, maxTemperature = {1}, sowingMethod = '{2}', treatmentType = '{3}', harvestMethod = '{4}', [Crop].state = 'not_planted'",minTemperature,maxTemperature,sowingMethod,treatmentType,harvestMethod),string.Format("Id = {0}",cropID)));
		}

		public void DeleteCropFromDB(int cropID)
		{
			DatabaseConnection._instance().CommandToDatabase(Constants.DeleteFromWhereByID("Crop",cropID));
		}

		public int GetCropID(List<Crop> crops, int index)
		{
			if (index >= 0)
			{
				List<int> cropIDs = new List<int>();

				foreach (var crop in crops)
				{
					cropIDs.Add(crop.ID);
				}
				return cropIDs[index];
			}
			return -1;
		}

        public void UpdateCropInDB(int id, string newState)
        {
            DatabaseConnection._instance().CommandToDatabase(Constants.UpdateSetWhere("Crop", string.Format("state = '{0}'", newState), string.Format("Id = {0}", id)));
        }

        public void CompleteRequest(int id)
        {
            //Always returns single result, so index 0 will be used only
            List<Crop> crops = DatabaseConnection._instance().GetCropDataSetAsList(Constants.SelectFromJoinWhere(cropFields, "Crop", "Container_Types", "container_TypeID = [Container_Types].[Id]", string.Format("Crop.Id = {0}", id)));

            //Update the crop state from 'requested' to 'seeds'
            UpdateCropInDB(crops[0].ID, "seeds");
        }

        public Crop GetCropByID(int cropID)
        {
            List<Crop> crops = DatabaseConnection._instance().GetCropDataSetAsList(Constants.SelectFromJoinWhere(cropFields, "Crop", "Container_Types", "container_TypeID = [Container_Types].[Id]", string.Format("[Crop].Id = {0}",cropID)));
            return crops[0];
        }

        public List<string> GetCropTypes()
        {
            return DatabaseConnection._instance().GetDataSetAsStringList(Constants.SelectFrom("DISTINCT type","Crop"));
        }

        public List<string> GetCropNames(string type)
        {
            return DatabaseConnection._instance().GetDataSetAsStringList(Constants.SelectFromWhere("DISTINCT name", "Crop", string.Format("type = '{0}'",type)));
        }

        public List<Crop> GetCropsByState(string state)
        {
            if (state == "Crop - In Stock")
            {
                return DatabaseConnection._instance().GetCropDataSetAsList(Constants.SelectFromJoinWhere(cropFields, "Crop", "Container_Types", "container_TypeID = [Container_Types].[Id]", "[Crop].[state] = 'in_stock'"));
            }
            else if (state == "Crop - Cultivating")
            {
                return DatabaseConnection._instance().GetCropDataSetAsList(Constants.SelectFromWhere("", "Crop", "state = 'cultivating'"));
            }
            else if (state == "Crop - Not Planted")
            {
                return DatabaseConnection._instance().GetCropDataSetAsList(Constants.SelectFromJoinWhere(cropFields, "Crop", "Container_Types", "container_TypeID = [Container_Types].[Id]", "[Crop].[state] = 'not_planted'"));
            }
            else if (state == "Crop - Seeds")
            {
                return DatabaseConnection._instance().GetCropDataSetAsList(Constants.SelectFromJoinWhere(cropFields, "Crop", "Container_Types", "container_TypeID = [Container_Types].[Id]", "[Crop].[state] = 'seeds'"));
            }
            else if (state == "requested")
            {
                return DatabaseConnection._instance().GetCropDataSetAsList(Constants.SelectFromJoinWhere(cropFields, "Crop", "Container_Types", "container_TypeID = [Container_Types].[Id]", "[Crop].[state] = 'requested' AND [numOfContainers] > 0"));
            }
            else
            {
                return null;
            }
        }
    }
}
