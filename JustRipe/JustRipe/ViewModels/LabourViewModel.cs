﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using JustRipe.Models;
using JustRipe.Database;
using System.Windows;

namespace JustRipe.ViewModels
{
    class LabourViewModel
    {
        private static String vehicleLabourFields = "[Labour].[Id],[User].[firstName], [User].[lastName], " +
                              "[Vehicle].[name], [Vehicle].[type], [Crop].[name], " +
                              "treatmentType ,labourType, numberOfLabourersNeeded, labourDate ";

        private static String manualLabourFields = "[Labour].[Id], [User].[firstName], [User].[lastName]," +
                              " [Crop].[name], treatmentType, labourType, numberOfLabourersNeeded, labourDate ";

        public List<Labour> GetLabours(string labourType, string labourMethod, string condition)
        {

            if (labourMethod == "Vehicle")
            {
                return DatabaseConnection._instance().GetLabourDataSetAsList(Constants.SelectFromJoinX4(
                    vehicleLabourFields, "Users-Labours", "User", "[User].[Id] = [Users-Labours].[userID]", "Labour",
                    "[Users-Labours].[labourID] = [Labour].[Id]", "Vehicle",
                    "[Labour].[vehicleID] = [Vehicle].[Id]", "Crop", string.Format("[Labour].[cropID] = [Crop].[Id] WHERE [Labour].[labourType] = '{0}' AND {1} AND labourDate > '01/01/2018' ORDER BY [Labour].[labourDate] DESC, [Labour].[labourType] ASC", labourType, condition)));
            }
            else if (labourMethod == "Manual")
            {
                return DatabaseConnection._instance().GetLabourDataSetAsList(Constants.SelectFromJoinX3(
                    manualLabourFields, "Users-Labours", "User", "[User].[Id] = [Users-Labours].[userID]", "Labour",
                    "[Users-Labours].[labourID] = [Labour].[Id]", "Crop", 
                    string.Format("[Labour].[cropID] = [Crop].[Id] WHERE [Labour].[labourType] = '{0}' AND {1} AND labourDate > '01/01/2018' AND [Labour].[vehicleID] IS NULL ORDER BY [Labour].[labourDate] DESC, [Labour].[labourType] ASC", labourType, condition)));
            }
            else
            {
                return null;
            }
        }

        public void AddLabourToBD(int vehicleID, int cropID, string labourType, int numberOfLabourersNeeded, string labourDate, int labourerID1, int labourerID2)
        {
            DatabaseConnection._instance().CommandToDatabase(Constants.InsertToLabour(vehicleID,cropID,labourType,numberOfLabourersNeeded,labourDate, labourerID1, labourerID2));
        }
    }
}
