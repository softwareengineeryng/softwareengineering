﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using JustRipe.Models;
using JustRipe.Database;

namespace JustRipe.ViewModels
{
    class VehicleViewModel
    {
        /// <summary>
        /// Method for adding the Vehicle to the Database
        /// </summary>
        /// <param name="vehicleName"></param>
        /// <param name="vehicleType"></param>
        public void AddVehicleToDB(string vehicleName, string vehicleType)
        {
            DatabaseConnection._instance().CommandToDatabase(Constants.InsertToVehicle(vehicleName, vehicleType));
        }

        /// <summary>
        /// Method for deleting the vehicle using vehicleID
        /// </summary>
        /// <param name="vehicleID"></param> unique ID
        public void DeleteVehicleFromDB(int vehicleID)
        {
                DatabaseConnection._instance().CommandToDatabase(Constants.DeleteFromVehicle(vehicleID));
        }

        /// <summary>
        /// Method for getting the vehicle's ID
        /// </summary>
        /// <param name="vehicles"></param> List of Vehicles
        /// <param name="index"></param> Index of chosen vehicle on DataGrid
        /// <returns></returns>
        public int GetVehicleID(List<Vehicle> vehicles, int index)
        {
            if (index >= 0)
            {
                List<int> vehicleIDs = new List<int>();

                foreach (var vehicle in vehicles)
                {
                    vehicleIDs.Add(vehicle.ID);
                }
                return vehicleIDs[index];
            }
            return -1;
        }

        /// <summary>
        /// Method for getting the list of all vehicles from the database
        /// </summary>
        /// <returns></returns>
        public List<Vehicle> GetVehicles()
        {

            return DatabaseConnection._instance().GetVehicleDataSetAsList(Constants.SelectFrom("", "Vehicle"));
        }

        /// <summary>
        /// Method for getting the list of all vehicles from the database 
        /// with availability depending on the chosen date
        /// </summary>
        /// <returns></returns>
        public List<Vehicle> GetVehicles(string date)
        {
            return CheckAvailability(date);
        }

        private List<Vehicle> CheckAvailability(string date)
        {
            List<Vehicle> vehicles = DatabaseConnection._instance().GetVehicleDataSetAsList(Constants.SelectFrom("", "Vehicle"));

            foreach (Vehicle vehicle in vehicles)
            {
                //Checks if vehicle is assigned to the task on a particular date
                string availability = DatabaseConnection._instance().GetDataSetAsString(Constants.SelectFromJoinWhere("Labour.Id","Labour","Vehicle","Labour.vehicleID = Vehicle.Id",string.Format("vehicleID = {0} and labourDate = '{1}'", vehicle.ID, date)));

                //if string is empty, vehicle is available
                if (availability == "")
                {
                    vehicle.Status = "Available";
                }
                //if string is not empty, vehicle is busy
                else
                {
                    vehicle.Status = "Busy";
                }
            }
            return vehicles;
        }
    }
}
