﻿using System.ComponentModel;

namespace JustRipe.Models
{
    class Vehicle
    {
        #region Attributes
        public int ID { get;  set; }
        public string Name { get;  set; }
        public string Type { get;  set; }
        public string Status { get; set; }
        #endregion
    }
}
