﻿using System;
using System.ComponentModel;

namespace JustRipe.Models
{
    class Crop
    {
        #region Attributes
        public int ID { get;  set; }
        public string Name { get;  set; }
        public string State { get;  set; }
        public string Type { get;  set; }
        public int MinTemperature { get;  set; }
        public int MaxTemperature { get;  set; }
        public string ContainerType { get;  set; }
        public int Capacity { get;  set; }
        public string UnitType { get;  set; }
        public int NumOfContainers { get;  set; }
        public string SowingMethod { get; set; }
        public string TreatmentType { get; set; }
        public string HarvestMethod { get; set; }
        #endregion
    }
}
