﻿using System;
using System.ComponentModel;

namespace JustRipe.Models
{
    class Attendance
    {
        #region Attributes
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Date { get; set; }
        #endregion
    }
}
