﻿using System.ComponentModel;

namespace JustRipe.Models
{
    class Field
    {
        #region Attributes
        public int ID { get; set; }
        public string CropName { get; set; }
        public string Type { get; set; }
        public string SowingMethod { get; set; }
        public string TreatmentType { get; set; }
        public string HarvestMethod { get; set; }
        #endregion
    }
}