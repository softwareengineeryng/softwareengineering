﻿using System;
using System.ComponentModel;

namespace JustRipe.Models
{
    class Labour
    {
        #region Attributes
        public int ID { get; set; }
        public string LabourerName{ get; set; }
        public string CropName { get; set; }
        public string FertiliserType { get; set; }
        public string LabourType { get; set; }
        public string VehicleName { get; set; }
        public string LabourDate { get; set; }
        public int NumbOfLabourers { get; set; }
        #endregion
    }
}
