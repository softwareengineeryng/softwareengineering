﻿using System.ComponentModel;

namespace JustRipe.Models
{
    class Fertiliser
    {
        #region Attributes
        public int ID { get; set; }
        public string State { get; set; }
        public string Type { get; set; }
        public int Amount { get; set; }
        #endregion
    }
}
