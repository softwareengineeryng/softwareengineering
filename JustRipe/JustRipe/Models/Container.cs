﻿using System.ComponentModel;

namespace JustRipe.Models
{
    class Container
    {
        #region Attributes
        public int ID { get; set; }
        public string Type { get; set; }
        public int Capacity { get; set; }
        public string UnitType { get; set; }
        public string State { get; set; }
        public int Amount { get; set; }
        #endregion
    }
}
