﻿using System.ComponentModel;

namespace JustRipe.Models
{
    class Role
    {
        #region Attributes
        public int ID { get; set; }
        public string Name { get; set; }
        #endregion
    }
}
