﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using JustRipe.Views;

namespace JustRipe.Utility
{
    /// <summary>
    /// This is a class with useful methods for other classes
    /// </summary>
    public class Helpers
    {

        /// <summary>
        /// Checks if a window with particular name or type is open, or both
        /// More details about how this method works is on link below
        /// Source: <https://stackoverflow.com/questions/16202101/how-do-i-know-if-a-wpf-window-is-opened>
        /// </summary>
        /// <typeparam name="T"> type of window </typeparam>
        /// <param name="name"> name of a window</param>
        /// <returns></returns>
        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }

        /// <summary>
        /// Method for opening a window of given object
        /// </summary>
        /// <param name="newPage"></param> instance of a window, that is needed to be opened
        public static void OpenWindow(object newPage)
        {
            //Closing the MainWindow
            App.Current.MainWindow.Close();

            //Assigning the new page as MainWindow
            App.Current.MainWindow = newPage as Window;

            //Showing the MainWindow back
            App.Current.MainWindow.Show();
        }

        /// <summary>
        /// Closes all windows, except main window
        /// </summary>
        public static void CloseWindows()
        {
            foreach (Window win in App.Current.Windows)
            {
                if (win.GetType() != App.Current.MainWindow.GetType())
                    win.Close();
            }
        }

        /// <summary>
        /// Method for encrypting the text passed to it
        /// Idea and most part of code were taken from online
        /// Source: <https://chandradev819.wordpress.com/2011/04/11/how-to-encrypt-and-decrypt-password-in-asp-net-using-c/>
        ///Accessed 30 November 2018
        /// </summary>
        /// <param name="plainText"></param> password given by user
        /// <returns></returns>
        public static string ComputeHash(string plainText, byte[] saltBytes)
        {
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
            new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

            SHA384Managed hash = new SHA384Managed();

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
            saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];
            
            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);
            
            // Return the result.
            return hashValue;
        }

        /// <summary>
        /// Method for verification of password by encrypting it and comparing with value in database
        /// Idea and most part of code were taken from online
        /// Source: <https://chandradev819.wordpress.com/2011/04/11/how-to-encrypt-and-decrypt-password-in-asp-net-using-c/>
        /// Accessed 30 November 2018
        /// </summary>
        /// <param name="plainText"></param> password entered by user
        /// <param name="hashValue"></param> encrypted password in the database
        /// <returns></returns>
        public static bool VerifyHash(string plainText, string hashValue)
        {
            // Convert base64-encoded hash value into a byte array.
            byte[] hashWithSaltBytes = Convert.FromBase64String(hashValue);

            // hash size in bytes
            int hashSizeInBytes = 48;
            
            // Make sure that the specified hash value is long enough.
            if (hashWithSaltBytes.Length < hashSizeInBytes)
                return false;

            // Allocate array to hold original salt bytes retrieved from hash.
            byte[] saltBytes = new byte[hashWithSaltBytes.Length - hashSizeInBytes];
            
            // Copy salt from the end of the hash to the new array.
            for (int i = 0; i < saltBytes.Length; i++)
                saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

            // Compute a new hash string.
            string expectedHashString = ComputeHash(plainText,saltBytes);

            // If the computed hash matches the specified hash,
            // the plain text value must be correct.
            return (hashValue == expectedHashString);
        }
    }
}
