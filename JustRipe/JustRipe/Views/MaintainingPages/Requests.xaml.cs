﻿using System.Windows;
using JustRipe.Utility;
using JustRipe.ViewModels;
using System.Windows.Controls;
using JustRipe.Views.LoginAndMPs;

namespace JustRipe.Views.MaintainingPages
{
    /// <summary>
    /// Interaction logic for Requests.xaml
    /// </summary>
    public partial class Requests : Window
    {
        public Requests()
        {
            InitializeComponent();
            ComboBox.SelectedIndex = 0;
		}

		private void GoBackButton_Click(object sender, RoutedEventArgs e)
		{
			//Opening the Manager page
			Helpers.OpenWindow(new ManagerMP());

			//Closing unnecessary windows
			Helpers.CloseWindows();
		}

		private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
            string stateOfItem = "requested";
            //if Crops chosen
			if (ComboBox.SelectedIndex == 0)
			{
                CropViewModel cropVM = new CropViewModel();
                DataGrid.ItemsSource = cropVM.GetCropsByState(stateOfItem);
			}
            //else if Fertiliser chosen
			else if (ComboBox.SelectedIndex == 1)
			{
                FertiliserViewModel fertiliserVM = new FertiliserViewModel();
				DataGrid.ItemsSource = fertiliserVM.GetFertilisersByState(stateOfItem);
			}
            //else if Containers chosen
            else if (ComboBox.SelectedIndex == 2)
            {
                ContainerViewModel containerVM = new ContainerViewModel();
                DataGrid.ItemsSource = containerVM.GetContainersByState(stateOfItem);
            }
		}

        private void CompleteRequestButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid.SelectedIndex == -1)
            {
                MessageBox.Show("Please select request that you want to complete before pressing the button", "Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                //Getting rid of unnecessary string "System..."
                string requestedItem = ((ComboBox.SelectedItem.ToString()).Substring(47, ComboBox.SelectedItem.ToString().Length - 47)).Trim();

                CompleteRequest(requestedItem);
            }
        }

        private void CompleteRequest(string requestedItem)
        {
            string stateOfItem = "requested";

            if (requestedItem == "Crops")
            {
                CropViewModel cropVM = new CropViewModel();
                int cropID = cropVM.GetCropID(cropVM.GetCropsByState(stateOfItem), DataGrid.SelectedIndex);

                //Check if returned ID is greater or equal 0 that verifies existence of crop
                if (cropID >= 0)
                {
                    cropVM.CompleteRequest(cropID);
                    MessageBox.Show("Request completed", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    DataGrid.ItemsSource = cropVM.GetCropsByState(stateOfItem); ;
                }
            }
            else if (requestedItem == "Fertilisers")
            {
                FertiliserViewModel fertiliserVM = new FertiliserViewModel();
                int fertiliserID = fertiliserVM.GetFertiliserID(fertiliserVM.GetFertilisersByState(stateOfItem), DataGrid.SelectedIndex);

                //Check if returned ID is greater or equal 0 that verifies existence of fertiliser
                if (fertiliserID >= 0)
                {
                    fertiliserVM.CompleteRequest(fertiliserID);
                    MessageBox.Show("Request completed", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    DataGrid.ItemsSource = fertiliserVM.GetFertilisersByState(stateOfItem); ;
                }
            }
            else if (requestedItem == "Containers")
            {
                ContainerViewModel containerVM = new ContainerViewModel();
                int containerID = containerVM.GetContainerID(containerVM.GetContainersByState(stateOfItem), DataGrid.SelectedIndex);

                //Check if returned ID is greater or equal 0 that verifies existence of fertiliser
                if (containerID >= 0)
                {
                    containerVM.CompleteRequest(containerID);
                    MessageBox.Show("Request completed", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    DataGrid.ItemsSource = containerVM.GetContainersByState(stateOfItem); ;
                }
            }
        }
    }
}
