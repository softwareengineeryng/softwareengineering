﻿using System.Windows;
using System.Windows.Controls;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Views.LoginAndMPs;

namespace JustRipe.Views.MaintainingPages
{
    /// <summary>
    /// Interaction logic for Timetable.xaml
    /// </summary>
    public partial class Timetable : Window
    {
        private object _accessor;

        #region Constructor

        /// <summary>
        /// Default constructor for Timetable page
        /// </summary>
        /// <param name="accessor"></param> page of the accessor (Manager or Labourer)
        public Timetable(object accessor)
        {
            InitializeComponent();
            _accessor = accessor;

            LabourTypeComboBox.SelectedIndex = 0;
        }
        #endregion
        
        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            //Going back to the same type of page as accessor
            if (_accessor.GetType() == typeof(LabourerMP))
            {
                //Opening the Labourer page
                Helpers.OpenWindow(new LabourerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
            else if (_accessor.GetType() == typeof(ManagerMP))
            {
                //Opening the Manager page
                Helpers.OpenWindow(new ManagerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
        }    //ADD UNASSIGNED LABOURS

        private void LabourTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //To trigger event, selected index will be changed twice in a row
            LabourMethodComboBox.SelectedIndex = 1;
            LabourMethodComboBox.SelectedIndex = 0;
        }

        private void LabourMethodComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Getting rid of unnecessary string "System..."
            string labourType = ((LabourTypeComboBox.SelectedItem.ToString()).Substring(38, LabourTypeComboBox.SelectedItem.ToString().Length - 38)).ToLower();
            string labourMethod = (LabourMethodComboBox.SelectedItem.ToString()).Substring(38, LabourMethodComboBox.SelectedItem.ToString().Length - 38);

            //Setting condition, for which labourers timetable should be shown
            //If user has his ID as 0, it means, it is one of the managers
            //First we get the userID and Role of the user from UserViewModel
            int userID = UserViewModel.UserID;
            string role = UserViewModel.Role;

            //Setting condition, depending who is accessing
            string condition = (role == "Manager") ? "[Users-Labours].[userID] IS NOT NULL" : string.Format("[Users-Labours].[userID] = {0}", userID);

            LabourViewModel labourVM = new LabourViewModel();
            TimetableDataGrid.ItemsSource = labourVM.GetLabours(labourType, labourMethod, condition);
        }
    }
}
