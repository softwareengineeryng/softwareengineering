﻿using System.Windows;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Database;
using JustRipe.Views.LoginAndMPs;

namespace JustRipe.Views.MaintainingPages
{
    /// <summary>
    /// Interaction logic for StorageAvailability.xaml
    /// </summary>
    public partial class StorageAvailability : Window
	{
        private object _accessor;
        #region Contructor
        
         /// <summary>
         /// Default Contructor for StorageAvailability
         /// </summary>
         /// <param name="accessor"></param> page of the accessor (Manager or Labourer)
        public StorageAvailability(object accessor)
        {
            InitializeComponent();
            _accessor = accessor;

            RefreshPage();
        }
        #endregion

        /// <summary>
        /// Method for refreshing the DataGrid of the page
        /// </summary>
        public void RefreshPage()
        {
            ContainerViewModel containerVM = new ContainerViewModel();
            ContainerDataGrid.ItemsSource = containerVM.GetContainersByState("In-stock");
        }

        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            //Going back to the same type of page as accessor
            if (_accessor.GetType() == typeof(LabourerMP))
            {
                //Opening the Labourer page
                Helpers.OpenWindow(new LabourerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
            else if (_accessor.GetType() == typeof(ManagerMP))
            {
                //Opening the Manager page
                Helpers.OpenWindow(new ManagerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
        }
	}
}
