﻿using System.Windows;
using JustRipe.Utility;
using JustRipe.ViewModels;
using System.Windows.Controls;
using JustRipe.Views.LoginAndMPs;
using JustRipe.Views.DialogPages;

namespace JustRipe.Views.MaintainingPages
{
	/// <summary>
	/// Interaction logic for CropList.xaml
	/// </summary>
	public partial class CropList : Window
	{
		#region Contructor

		/// <summary>
		/// Default Contructor for StaffList
		/// </summary>
		public CropList()
		{
			InitializeComponent();

			//DataGrid connected to show "All Crops" crops when opened
			ComboBox.SelectedIndex = 0;
		}
		#endregion

		private void GoBackButton_Click(object sender, RoutedEventArgs e)
		{
			//Opening the Manager page
			Helpers.OpenWindow(new ManagerMP());

			//Closing unnecessary windows
			Helpers.CloseWindows();
		}

		/// <summary>
		/// Connecting the different selection in the combobox to the datagrid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AssignTaskButton.Visibility = Visibility.Hidden;

            //Getting rid of unnecessary string "System..."
            string stateOfItem = (ComboBox.SelectedItem.ToString()).Substring(38, ComboBox.SelectedItem.ToString().Length - 38);

            if (stateOfItem == "Fertiliser - In stock")
            {
                FertiliserViewModel fertiliserVM = new FertiliserViewModel();
                CropDataGrid.ItemsSource = fertiliserVM.GetFertilisersByState(stateOfItem);
            }
            else
            {
                if (stateOfItem == "Crop - Seeds")
                {
                    AssignTaskButton.Visibility = Visibility.Visible;
                }
                CropViewModel cropVM = new CropViewModel();
                CropDataGrid.ItemsSource = cropVM.GetCropsByState(stateOfItem);
            }
        }

        private void AssignTaskButton_Click(object sender, RoutedEventArgs e)
        {
            //Checking if window is already opened
            //And preventing opening the same window several times
            if (!Helpers.IsWindowOpen<AssignTask>())
            {
                CropViewModel cropVM = new CropViewModel();

                int cropID = cropVM.GetCropID(cropVM.GetCropsByState("Crop - Seeds"), CropDataGrid.SelectedIndex);

                if (cropID >= 0)
                {
                    //Creating instance of AssignTask
                    AssignTask assignTask = new AssignTask(cropID);

                    //Showing the page
                    assignTask.Show();
                }
            }
        }
    }
}