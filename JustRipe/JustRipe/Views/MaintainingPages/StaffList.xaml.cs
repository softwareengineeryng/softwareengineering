﻿using System.Windows;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Models;
using System.Windows.Controls;
using System.Windows.Data;
using JustRipe.Views.LoginAndMPs;
using JustRipe.Views.DialogPages;

namespace JustRipe.Views.MaintainingPages
{
    /// <summary>
    /// Interaction logic for StaffList.xaml
    /// </summary>
    public partial class StaffList : Window
	{
        #region Contructor

        /// <summary>
        /// Default Contructor for StaffList
        /// </summary>
        ///
        public StaffList()
        {
            InitializeComponent();
            RefreshPage();
		}
        #endregion
        
        /// <summary>
        /// Method for refreshing the DataGrid of the page
        /// </summary>
        public void RefreshPage()
        {
            UserViewModel userVM = new UserViewModel();
            StaffListDataGrid.ItemsSource = userVM.GetUsers();
        }

        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {    
            //Opening the Manager page
            Helpers.OpenWindow(new ManagerMP());

            //Closing unnecessary windows
            Helpers.CloseWindows();
        }

        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            //Checking if window is already opened
            //And preventing opening the same window several times
            if (!Helpers.IsWindowOpen<AddUser>())
            {
                //Creating instance of ManageStaffList
                AddUser addUser = new AddUser();

                //Showing the page
                addUser.Show();
            }
        }

        private void RemoveUserButton_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel userVM = new UserViewModel();
            int userID = userVM.GetUserID(userVM.GetUsers(), StaffListDataGrid.SelectedIndex);
            if (userID >= 0)
            {
                MessageBoxResult result = MessageBox.Show("Do you want to delete the user from the Database ?",
                                          "Confirmation",
                                          MessageBoxButton.YesNo,
                                          MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    userVM.DeleteUserFromDB(userID);
                    MessageBox.Show("User succesfully deleted", "Deletion");

                    //Refreshing the page after changes
                    RefreshPage();
                }
            }
        }

        /// <summary>
        /// Method opening the Attendance page, when Check Attendance button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckAttendance_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel userVM = new UserViewModel();
            int userID = userVM.GetUserID(userVM.GetUsers(), StaffListDataGrid.SelectedIndex);

            //Checking if appropriate user was selected
            if (userID >= 0)
            {
                //Opening attendance page using userID
                AttendancePage attendancePage = new AttendancePage(userID);
                attendancePage.Show();
            }
        }
    }
}
