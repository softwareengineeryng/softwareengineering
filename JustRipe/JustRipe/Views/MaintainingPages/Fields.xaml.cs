﻿using System.Windows;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Views.LoginAndMPs;

namespace JustRipe.Views.MaintainingPages
{
    /// <summary>
    /// Interaction logic for Fields.xaml
    /// </summary>
    public partial class Fields : Window
    {
        private object _accessor;
        #region Contructor

        /// <summary>
        /// Default Contructor for StorageAvailability
        /// </summary>
        /// <param name="accessor"></param> page of the accessor (Manager or Labourer)
        public Fields(object accessor)
        {
            InitializeComponent();
            _accessor = accessor;

            ComboBox.SelectedIndex = 0;
        }
        #endregion
        
        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            //Going back to the same type of page as accessor
            if (_accessor.GetType() == typeof(LabourerMP))
            {
                //Opening the Labourer page
                Helpers.OpenWindow(new LabourerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
            else if (_accessor.GetType() == typeof(ManagerMP))
            {
                //Opening the Manager page
                Helpers.OpenWindow(new ManagerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
        }

        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //Getting rid of unnecessary string "System..."
            string stateOfItem = ((ComboBox.SelectedItem.ToString()).Substring(38, ComboBox.SelectedItem.ToString().Length - 38)).ToLower();

            FieldViewModel fertiliserVM = new FieldViewModel();
            FieldDataGrid.ItemsSource = fertiliserVM.GetFieldsByState(stateOfItem);
        }
    }
}
