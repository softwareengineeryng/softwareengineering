﻿using System.Windows;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Views.LoginAndMPs;
using JustRipe.Views.DialogPages;
using System;

namespace JustRipe.Views.MaintainingPages
{
    /// <summary>
    /// Interaction logic for VehiclesAvailability.xaml
    /// </summary>
    public partial class VehiclesAvailability : Window
	{
        private object _accessor;
        #region Contructor

        /// <summary>
        /// Default Contructor for VehiclesAvailability
        /// </summary>
        public VehiclesAvailability(object accessor)
		{
			InitializeComponent();
            _accessor = accessor;
            DatePicker.SelectedDate = DateTime.Now;
            RefreshPage();

            //Altering the page, depending who has accesed to it
            //Labourers are not able to add or remove anything from the database
            AddVehicleButton.Visibility = (_accessor.GetType() == typeof(LabourerMP)) ? Visibility.Hidden : Visibility.Visible;
            RemoveVehicleButton.Visibility = (_accessor.GetType() == typeof(LabourerMP)) ? Visibility.Hidden : Visibility.Visible;
            Height = (_accessor.GetType() == typeof(LabourerMP)) ? 380 : 500;
        }

        #endregion

        /// <summary>
        /// Method for refreshing the DataGrid of the page
        /// </summary>
        public void RefreshPage()
        {
            VehicleViewModel vehicleVM = new VehicleViewModel();
            VehicleDataGrid.ItemsSource = vehicleVM.GetVehicles(Convert.ToDateTime(DatePicker.SelectedDate).ToString("yyyy-MM-dd"));
        }


        private void GoBackButton_Click(object sender, RoutedEventArgs e)
        {
            if (_accessor.GetType() == typeof(LabourerMP))
            {
                //Opening the Labourer page
                Helpers.OpenWindow(new LabourerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
            else if (_accessor.GetType() == typeof(ManagerMP))
            {
                //Opening the Manager page
                Helpers.OpenWindow(new ManagerMP());

                //Closing unnecessary windows
                Helpers.CloseWindows();
            }
        }

        /// <summary>
        /// Method for opening AddVehicle form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddVehicleButton_Click(object sender, RoutedEventArgs e)
        {
            //Checking if window is already opened
            //And preventing opening the same window several times
            if (!Helpers.IsWindowOpen<AddVehicle>())
            {
                //Creating instance of AddVehicle
                AddVehicle addVehicle = new AddVehicle();

                //Showing the page
                addVehicle.Show();
            }
        }

        /// <summary>
        /// Method for deleting Vehicle from the Database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveVehicleButton_Click(object sender, RoutedEventArgs e)
        {
            VehicleViewModel vehicleVM = new VehicleViewModel();
            int vehicleID = vehicleVM.GetVehicleID(vehicleVM.GetVehicles(), VehicleDataGrid.SelectedIndex);
            if (vehicleID >= 0)
            {
                MessageBoxResult result = MessageBox.Show("Do you want to delete the vehicle from the Database ?",
                                          "Confirmation",
                                          MessageBoxButton.YesNo,
                                          MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    vehicleVM.DeleteVehicleFromDB(vehicleID);
                    MessageBox.Show("Vehicle succesfully deleted", "Deletion");
                    RefreshPage();
                }
            }
        }

        /// <summary>
        /// Method for refreshing the page when date in the DatePicker has been changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DatePicker_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //Refreshing the page and show data depending on the chosen date
            RefreshPage();
        }
    }
}
