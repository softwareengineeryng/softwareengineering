﻿using JustRipe.Utility;
using JustRipe.ViewModels;
using System.Windows;

namespace JustRipe.Views.LoginAndMPs
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Window
    {
        #region Contructor

        /// <summary>
        /// Default Contructor for LoginPage
        /// </summary>
        public LoginPage()
        {
            InitializeComponent();
        }
        #endregion
        
        /// <summary>
        /// Event for logging in when button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {   //Getting the page of the user, depending on his/her role
            object result = new UserViewModel().CheckCredentials(UsernameTextBox.Text, PasswordBox.Password);
            
            //Checking if username and passwor is correct
            if (result != null)
            {
                //Hiding the error label
                ErrorTextBlock.Visibility = Visibility.Hidden;

                //Opening the page
                Helpers.OpenWindow(result);
            }
            else
            {
                //Showing the error label
                ErrorTextBlock.Visibility = Visibility.Visible;

                //Clearing the username and password fields
                UsernameTextBox.Text = "";
                PasswordBox.Password = "";
            }
        }

        /// <summary>
        /// Event for hiding error label when another item selected apart from button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginButton_LostFocus(object sender, RoutedEventArgs e)
        {
            //Hiding the error label
            ErrorTextBlock.Visibility = Visibility.Hidden;
        }
    }
}
