﻿using JustRipe.Utility;
using System.Windows;
using System.Windows.Controls;
using JustRipe.Views.DialogPages;
using JustRipe.Views.MaintainingPages;

namespace JustRipe.Views.LoginAndMPs
{
    /// <summary>
    /// Interaction logic for ManagerMP.xaml
    /// </summary>
    public partial class ManagerMP : Window
    {
        #region Contructor

        /// <summary>
        /// Default Contructor for ManagerMP
        /// </summary>
        public ManagerMP()
        {
            InitializeComponent();
        }
        #endregion

        private void RequestButton_Click(object sender, RoutedEventArgs e)
        {
			//Shows the request Table from DB showing requests from labourers
			Helpers.OpenWindow(new Requests());
        }
        private void StorageButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the StorageAvailability page
            Helpers.OpenWindow(new StorageAvailability(this));
        }

        private void VehiclesButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the VehiclesAvailability page
            Helpers.OpenWindow(new VehiclesAvailability(this));
        }

        private void StaffListButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the StaffList page
            Helpers.OpenWindow(new StaffList());
        }

        private void CropListButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the CropList page
			Helpers.OpenWindow(new CropList());
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the Login page
            Helpers.OpenWindow(new LoginPage());

            //Closing all unnecessary windows(i.e. AddVehicle or AddUser and etc.)
            Helpers.CloseWindows();
        }

        private void TimetableButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the Timetable page
            Helpers.OpenWindow(new Timetable(this));
        }

        private void FieldsButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the Fields page
            Helpers.OpenWindow(new Fields(this));
        }

        private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            //Checking if window is already opened
            //And preventing opening the same window several times
            if (!Helpers.IsWindowOpen<ChangePassword>())
            {
                //Creating instance of ChangePassword
                ChangePassword changePassword = new ChangePassword();

                //Showing the page
                changePassword.Show();
            }
        }
    }
}
