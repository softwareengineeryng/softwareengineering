﻿using JustRipe.Utility;
using System.Windows;
using JustRipe.Views.DialogPages;
using JustRipe.Views.MaintainingPages;

namespace JustRipe.Views.LoginAndMPs
{
    /// <summary>
    /// Interaction logic for LabourerMP.xaml
    /// </summary>
    public partial class LabourerMP : Window
    {
        #region Contructor

        /// <summary>
        /// Default Contructor for LabourerMP
        /// </summary>
        public LabourerMP()
        {
            InitializeComponent();
        }
        #endregion

        private void RequestItemsButton_Click(object sender, RoutedEventArgs e)
        {
            //Checking if window is already opened
            //And preventing opening the same window several times
            if (!Helpers.IsWindowOpen<RequestItemPage>())
            {
                //Creating instance of RequestItemPage
                RequestItemPage requestItemPage = new RequestItemPage();

                //Showing the page
                requestItemPage.Show();
            }
        }

        private void StorageButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the StorageAvailability page
            Helpers.OpenWindow(new StorageAvailability(this));
        }

        private void VehiclesButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the VehiclesAvailability page
            Helpers.OpenWindow(new VehiclesAvailability(this));
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the Login page
            Helpers.OpenWindow(new LoginPage());

            //Closing all unnecessary windows(RequestItemPage and etc.)
            Helpers.CloseWindows();
        }

        private void TimetableButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the Timetable page
            Helpers.OpenWindow(new Timetable(this));
        }

        private void FieldsButton_Click(object sender, RoutedEventArgs e)
        {
            //Opening the Fields page
            Helpers.OpenWindow(new Fields(this));
        }

        private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            //Checking if window is already opened
            //And preventing opening the same window several times
            if (!Helpers.IsWindowOpen<ChangePassword>())
            {
                //Creating instance of ChangePassword
                ChangePassword changePassword = new ChangePassword();

                //Showing the page
                changePassword.Show();
            }
        }
    }
}
