﻿using JustRipe.Models;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Views.MaintainingPages;
using System;
using System.Collections.Generic;
using System.Windows;

namespace JustRipe.Views.DialogPages
{
    /// <summary>
    /// Interaction logic for AssignTask.xaml
    /// </summary>
    public partial class AssignTask : Window
    {
        private int _cropID;
        public AssignTask(int cropID)
        {
            InitializeComponent();
            _cropID = cropID;
            PrefillFields();
        }

        private void PrefillFields()
        {
            CropViewModel cropVM = new CropViewModel();

            Crop crop = cropVM.GetCropByID(_cropID);

            nameTextBox.Text = crop.Name;
            stateTextBox.Text = crop.State;
            typeTextBox.Text = crop.Type;

            nameTextBox.IsEnabled = false;
            stateTextBox.IsEnabled = false;
            typeTextBox.IsEnabled = false;
        }

        private void AssignTaskButton_Click(object sender, RoutedEventArgs e)
        {
            //Checking if all TextBoxes are filled out
            if (sowingMethodTextBox.Text != "" &&
                treatmentTypeTextBox.Text != "" &&
                harvestMethodTextBox.Text != "" &&
                minTemperatureTextBox.Text.ToString() != "" &&
                maxTemperatureTextBox.Text.ToString() != "")
            {
                //Hiding error label, because all fields are filled out
                ErrorLabel.Visibility = Visibility.Hidden;

                CropViewModel cropVM = new CropViewModel();

                //Creating a crop in database with given data
                cropVM.UpdateCropDetails(_cropID,Convert.ToInt32(minTemperatureTextBox.Text), Convert.ToInt32(maxTemperatureTextBox.Text),sowingMethodTextBox.Text, treatmentTypeTextBox.Text, harvestMethodTextBox.Text);

                LabourDates labourDates = new LabourDates(_cropID);
                labourDates.Show();
            }
            else
            {
                //Showing the error label
                ErrorLabel.Visibility = Visibility.Visible;
            }
        }
    }
}
