﻿using JustRipe.Models;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Views.MaintainingPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JustRipe.Views.DialogPages
{
    /// <summary>
    /// Interaction logic for LabourDates.xaml
    /// </summary>
    public partial class LabourDates : Window
    {
        private int _cropID;
        public LabourDates(int cropID)
        {
            InitializeComponent();
            SetUpComboBoxes();
            _cropID = cropID;
        }

        private void SetUpComboBoxes()
        {
            UserViewModel userVM = new UserViewModel();
            List<User> labourers = userVM.GetLabourers(-1);

            foreach (User labourer in labourers)
            {
                firstLabourerComboBox.Items.Add(string.Concat(labourer.ID, ",",labourer.FirstName, ",", labourer.LastName));
            }
            secondLabourerComboBox.IsEnabled = false;
        }

        private void AssignTaskButton_Click(object sender, RoutedEventArgs e)
        {

            string sowingDate = Convert.ToDateTime(sowingDateDatePicker.SelectedDate.ToString()).ToString("yyyy-MM-dd");
            string harvestDate = Convert.ToDateTime(harvestDateDatePicker.SelectedDate.ToString()).ToString("yyyy-MM-dd");

            if (sowingDate == "" && harvestDate != "")
            {
                if (firstLabourerComboBox.SelectedIndex >= 0)
                {
                    int labourerID1 = Convert.ToInt32((firstLabourerComboBox.SelectedItem.ToString()).Substring(0, 1));

                    LabourViewModel labourVM = new LabourViewModel();

                    if(secondLabourerComboBox.SelectedIndex >= 0)
                    {
                        int labourerID2 = Convert.ToInt32((secondLabourerComboBox.SelectedItem.ToString()).Substring(0, 1));


                        labourVM.AddLabourToBD(2, _cropID, "sowing",2,sowingDate,labourerID1,labourerID2);
                        labourVM.AddLabourToBD(2, _cropID, "harvesting",2,harvestDate,labourerID1,labourerID2);
                    }
                    else
                    {
                        labourVM.AddLabourToBD(2, _cropID, "sowing", 1, sowingDate, labourerID1, -1);
                        labourVM.AddLabourToBD(2, _cropID, "harvesting", 1, harvestDate, labourerID1, -1);
                    }
                }
                else
                {
                    ErrorLabel.Visibility = Visibility.Visible;
                }
            }

            //MessageBox with confirmation of task being assigned
            MessageBox.Show("Task Assigned successfully", "Confirmation");

            //Opening the CropList page
            Helpers.OpenWindow(new CropList());

            //Closing the unnecessary windows
            Helpers.CloseWindows();
        }

        private void AssignTaskButton_LostFocus(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Visibility = Visibility.Hidden;
        }

        private void firstLabourerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            secondLabourerComboBox.IsEnabled = true;

            try
            {
                int labourerID = Convert.ToInt32((firstLabourerComboBox.SelectedItem.ToString()).Substring(0, 1));

                UserViewModel userVM = new UserViewModel();
                List<User> labourers = userVM.GetLabourers(labourerID);

                foreach (User labourer in labourers)
                {
                    secondLabourerComboBox.Items.Add(string.Concat(labourer.ID, ",", labourer.FirstName, ",", labourer.LastName));
                }
            }
            catch
            {
                //For catching unwilled errors
            }
        }
    }
}
