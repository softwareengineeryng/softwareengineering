﻿using System.Windows;
using JustRipe.Utility;
using JustRipe.ViewModels;
using JustRipe.Views.MaintainingPages;

namespace JustRipe.Views.DialogPages
{
    /// <summary>
    /// Interaction logic for AddUser.xaml
    /// </summary>
    public partial class AddUser : Window
    {
        public AddUser()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event for adding user to the database after button was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            //Hiding user added label
            userAddedLabel.Visibility = Visibility.Hidden;

            //Checking if any of the text boxes or passwordbox contains single quote
            if (firstNameTextBox.Text.IndexOf("'") != -1 || lastNameTextBox.Text.IndexOf("'") != -1 || emailTextBox.Text.IndexOf("'") != -1 || phoneTextBox.Text.IndexOf("'") != -1 || usernameTextBox.Text.IndexOf("'") != -1 || passwordPasswordBox.Password.IndexOf("'") != -1)
            {
                ErrorLabel.Content = "Please do not use single quote (')";

                //Showing the error label
                ErrorLabel.Visibility = Visibility.Visible;
            }
            //Checking if all TextBoxes and Radio Buttons are filled out
            else if (firstNameTextBox.Text != "" && 
                lastNameTextBox.Text != "" &&
                emailTextBox.Text != "" && 
                phoneTextBox.Text != "" && 
                usernameTextBox.Text != "" && 
                passwordPasswordBox.Password != "" &&
                (labourerRadioButton.IsChecked == true || managerRadioButton.IsChecked == true))
            {
                //Hiding error label, because all fields are filled out
                ErrorLabel.Visibility = Visibility.Hidden;

                UserViewModel user = new UserViewModel();

                //Encrypting user's password
                string encryptedPassword = Helpers.ComputeHash(passwordPasswordBox.Password,null);

                //Creating a user in database with given data
                user.AddUserToDB(firstNameTextBox.Text, lastNameTextBox.Text, emailTextBox.Text, phoneTextBox.Text, usernameTextBox.Text, encryptedPassword, (bool)managerRadioButton.IsChecked);

                //Clearing all texboxes and unchecking radio buttons
                userAddedLabel.Visibility = Visibility.Visible;
                firstNameTextBox.Text = "";
                lastNameTextBox.Text = "";
                emailTextBox.Text = "";
                phoneTextBox.Text = "";
                usernameTextBox.Text = "";
                passwordPasswordBox.Password = "";
                labourerRadioButton.IsChecked = false;
                managerRadioButton.IsChecked = false;

                //Opening the StaffList page
                Helpers.OpenWindow(new StaffList());
            }
            else
            {
                ErrorLabel.Content = "Please fill in all fields";

                //Showing the error label
                ErrorLabel.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Event for defining, if user selected other item apart from add user button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddUserButton_LostFocus(object sender, RoutedEventArgs e)
        {
            //Hiding user added label after other item selected
            userAddedLabel.Visibility = Visibility.Hidden;
        }
    }
}
