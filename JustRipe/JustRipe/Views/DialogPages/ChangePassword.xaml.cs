﻿using JustRipe.ViewModels;
using System.Windows;

namespace JustRipe.Views.DialogPages
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Window
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            //Hiding all labels from the screen
            HideLabels();
            
            //Checking if all fields are filled in
            if (oldPasswordBox.Password == "" || confirmPasswordBox.Password == "" || newPasswordBox.Password == "")
            {
                //Showing error message, asking for filling the fields
                EmptyFieldErrorLabel.Visibility = Visibility.Visible;
            }
            else
            {
                UserViewModel userVM = new UserViewModel();

                //Comparing the input password, with one in the database
                if (userVM.ComparePasswords(oldPasswordBox.Password))
                {
                    //check if new password and confirmation password matches
                    if (newPasswordBox.Password == confirmPasswordBox.Password)
                    {
                        //Update the password in the database
                        userVM.UpdatePassword(newPasswordBox.Password);

                        //Show message with successful change of password
                        PasswordChangedLabel.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        //Show error message, asking for new and confirmation passwords to be same
                        IncorrectConfimationLabel.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    //Show error message asking for correct old password
                    IncorrectOldPasswordLabel.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Method for hiding all labels from the screen
        /// </summary>
        private void HideLabels()
        {
            IncorrectConfimationLabel.Visibility = Visibility.Hidden;
            EmptyFieldErrorLabel.Visibility = Visibility.Hidden;
            IncorrectOldPasswordLabel.Visibility = Visibility.Hidden;
            PasswordChangedLabel.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Method for defining, if user starts to focus on other item
        /// So, all labels can be hided
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangePasswordButton_LostFocus(object sender, RoutedEventArgs e)
        {
            //Hiding all labels from the screen
            HideLabels();
        }
    }
}
