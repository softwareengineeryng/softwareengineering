﻿using JustRipe.Utility;
using JustRipe.ViewModels;
using System.Windows;
using JustRipe.Views.LoginAndMPs;
using JustRipe.Views.MaintainingPages;

namespace JustRipe.Views.DialogPages
{
    /// <summary>
    /// Interaction logic for AddVehicle.xaml
    /// </summary>
    public partial class AddVehicle : Window
    {
        public AddVehicle()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event for adding crop to the database after button was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddVehicleButton_Click(object sender, RoutedEventArgs e)
        {
            //Hiding vehicle added label
            vehicleAddedLabel.Visibility = Visibility.Hidden;

            //Checking if all TextBoxes are filled out and does not contain single quote ('), to prevent SQL statement crush
            if (vehicleNameTextBox.Text.IndexOf("'") != -1 || vehicleTypeTextBox.Text.IndexOf("'") != -1)
            {
                ErrorLabel.Content = "Please do not use single quote (')";

                //Showing the error label
                ErrorLabel.Visibility = Visibility.Visible;
            }
            else if (vehicleNameTextBox.Text != "" && vehicleTypeTextBox.Text != "")
            {
                //Hiding error label, because all fields are filled out
                ErrorLabel.Visibility = Visibility.Hidden;

                VehicleViewModel vehicleVM = new VehicleViewModel();

                //Creating a vehicle in database with given data
                vehicleVM.AddVehicleToDB(vehicleNameTextBox.Text, vehicleTypeTextBox.Text);

                //Clearing all texboxes
                vehicleAddedLabel.Visibility = Visibility.Visible;
                vehicleNameTextBox.Text = "";
                vehicleTypeTextBox.Text = "";


                //Opening the StaffList page
                Helpers.OpenWindow(new VehiclesAvailability(new ManagerMP()));


            }
            else
            {
                ErrorLabel.Content = "Please fill in all fields";

                //Showing the error label
                ErrorLabel.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Event for defining, if user selected other item apart from add vehicle button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddVehicleButton_LostFocus(object sender, RoutedEventArgs e)
        {
            //Hiding user added label after other item selected
            vehicleAddedLabel.Visibility = Visibility.Hidden;
        }
    }
}
