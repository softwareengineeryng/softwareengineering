﻿using JustRipe.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JustRipe.Views.DialogPages
{
    /// <summary>
    /// Interaction logic for AttendancePage.xaml
    /// </summary>
    public partial class AttendancePage : Window
    {
        public AttendancePage(int userID)
        {
            InitializeComponent();
            AttendanceViewModel attendanceVM = new AttendanceViewModel();
            AttendanceDataGrid.ItemsSource = attendanceVM.GetAttendancesByUserID(userID);
        }
    }
}
