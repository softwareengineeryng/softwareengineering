﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using JustRipe.ViewModels;

namespace JustRipe.Views.DialogPages
{
    /// <summary>
    /// Interaction logic for RequestItemPage.xaml
    /// </summary>
    public partial class RequestItemPage : Window
    {
        #region Contructor

        /// <summary>
        /// Default Contructor for RequestItemPage
        /// </summary>
        public RequestItemPage()
        {
            InitializeComponent();
            TypeComboBox.IsEnabled = false;
            NameComboBox.IsEnabled = false;
        }
        #endregion

        
    private void ItemComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AmountTextBox.IsEnabled = true;
            ContainerTypeComboBox.IsEnabled = false;

            //Seeds
            if (ItemComboBox.SelectedIndex == 0)
            {
                CropViewModel cropVM = new CropViewModel();

                TypeComboBox.Items.Clear();
                TypeComboBox.IsEnabled = true;

                List<string> types = cropVM.GetCropTypes();

                for (int i = 0; i < types.Count; i++)
                {
                    TypeComboBox.Items.Add(types[i]);
                }

                NameTextBox.Text = "Name";
                ContainerTypeTextBox.Visibility = Visibility.Visible;
                NameTextBox.Visibility = Visibility.Visible;
                AmountTextBox.Clear();
            }
            //Containers
            else if (ItemComboBox.SelectedIndex == 1)
            {
                ContainerViewModel containerVM = new ContainerViewModel();

                TypeComboBox.IsEnabled = true;
                TypeComboBox.Items.Clear();

                List<string> types = containerVM.GetContainerTypes();
                

                for (int i = 0; i < types.Count; i++)
                {
                    TypeComboBox.Items.Add(types[i]);
                }

                NameTextBox.Text = "Capacity";
                ContainerTypeTextBox.Visibility = Visibility.Hidden;
                NameTextBox.Visibility = Visibility.Visible;
                AmountTextBox.Clear();
            }
            //Fertiliser
            else if (ItemComboBox.SelectedIndex == 2)
            {
                FertiliserViewModel fertiliserVM = new FertiliserViewModel();

                TypeComboBox.IsEnabled = true;
                TypeComboBox.Items.Clear();

                List<string> types = fertiliserVM.GetFertiliserTypes();

                for (int i = 0; i < types.Count; i++)
                {
                    TypeComboBox.Items.Add(types[i]);
                }

                NameTextBox.Text = "Name";
                ContainerTypeTextBox.Visibility = Visibility.Hidden;
                NameTextBox.Visibility = Visibility.Hidden;
                AmountTextBox.Clear();
                

            }
            else
            {
                NameComboBox.IsEnabled = false;
            }
        }
        private void TypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AmountTextBox.IsEnabled = true;
            ContainerTypeComboBox.IsEnabled = false;

            //If Seeds and Fruit
            if (TypeComboBox.SelectedIndex == 0 && ItemComboBox.SelectedIndex == 0)
            {
                NameComboBox.IsEnabled = true;
                CropViewModel cropVM = new CropViewModel();

                NameComboBox.Items.Clear();

                string type = TypeComboBox.SelectedValue.ToString();

                List<string> names = cropVM.GetCropNames(type);

                for (int i = 0; i < names.Count; i++)
                {
                    NameComboBox.Items.Add(names[i]);
                }
                
                AmountTextBox.Clear();
            }
            // If Seeds and Herb
            else if (TypeComboBox.SelectedIndex == 1 && ItemComboBox.SelectedIndex == 0)
            {
                NameComboBox.IsEnabled = true;
                CropViewModel cropVM = new CropViewModel();

                NameComboBox.Items.Clear();

                string type = TypeComboBox.SelectedValue.ToString();

                List<string> names = cropVM.GetCropNames(type);

                for (int i = 0; i < names.Count; i++)
                {
                    NameComboBox.Items.Add(names[i]);
                }
                
                AmountTextBox.Clear();
            }
            // If Seeds and Vegetable
            else if (TypeComboBox.SelectedIndex == 2 && ItemComboBox.SelectedIndex == 0)
            {

                CropViewModel cropVM = new CropViewModel();

                NameComboBox.IsEnabled = true;
                NameComboBox.Items.Clear();

                string type = TypeComboBox.SelectedValue.ToString();

                List<string> names = cropVM.GetCropNames(type);

                for (int i = 0; i < names.Count; i++)
                {
                    NameComboBox.Items.Add(names[i]);
                }
                
                AmountTextBox.Clear();
            }
            // If Box and Container selected
            else if (TypeComboBox.SelectedIndex == 0 && ItemComboBox.SelectedIndex == 1)
            {
                ContainerViewModel containerVM = new ContainerViewModel();
                
                NameComboBox.IsEnabled = true;
                NameComboBox.Items.Clear();

                List<string> types = containerVM.GetContainerCapacities("Box");


                for (int i = 0; i < types.Count; i++)
                {
                    NameComboBox.Items.Add(types[i]);
                }
                
                AmountTextBox.Clear();
            }
            // If Bag and Container selected
            else if (TypeComboBox.SelectedIndex == 1 && ItemComboBox.SelectedIndex == 1)
            {
                ContainerViewModel containerVM = new ContainerViewModel();
                
                NameComboBox.IsEnabled = true;
                NameComboBox.Items.Clear();

                List<string> capacities = containerVM.GetContainerCapacities("Bag");


                for (int i = 0; i < capacities.Count; i++)
                {
                    NameComboBox.Items.Add(capacities[i]);
                }
                
                AmountTextBox.Clear();
            }
            // If not Seeds or Containers, disable ComboBox
            else
            {
                NameComboBox.Items.Clear();
                NameComboBox.IsEnabled = false;
                SuccessLabel.Visibility = Visibility.Hidden;
            }

        }
        private void NameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ContainerTypeComboBox.IsEnabled = false;
            AmountTextBox.IsEnabled = true;
            SuccessLabel.Visibility = Visibility.Hidden;

            if(ItemComboBox.SelectedIndex == 0)
            {
                AmountTextBox.IsEnabled = false;
                ContainerTypeComboBox.IsEnabled = true;

                ContainerViewModel containerVM = new ContainerViewModel();

                ContainerTypeComboBox.IsEnabled = true;
                ContainerTypeComboBox.Items.Clear();

                List<string> capacities = containerVM.GetContainerCapacities("Bag");


                for (int i = 0; i < capacities.Count; i++)
                {
                    ContainerTypeComboBox.Items.Add(capacities[i]);
                }

                AmountTextBox.Clear();
            }
        }
        
        private void ContainerTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AmountTextBox.IsEnabled = true;
        }

        private void AmountTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SuccessLabel.Visibility = Visibility.Hidden;
            ErrorLabel.Visibility = Visibility.Hidden;
        }

        //Adding Method to input requested item into database -- Samuel
        //Auto-filled fields should be implemented
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
          if (AmountTextBox.Text != "")
            {
                try
                {
                    int amount = Convert.ToInt32(AmountTextBox.Text.ToString());
                    if (amount > 0)
                    {
                        if (ItemComboBox.SelectedIndex == 0)
                        {
                            RecordRequestCrop(amount);
                        }
                        else if (ItemComboBox.SelectedIndex == 1)
                        {
                            RecordRequestContainer(amount);
                        }
                        else if (ItemComboBox.SelectedIndex == 2)
                        {
                            RecordRequestFertiliser(amount);
                        }

                        SuccessLabel.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ErrorLabel.Content = "Please Enter a postive number.";
                        ErrorLabel.Visibility = Visibility.Visible;
                    }
                }
                catch
                {
                   ErrorLabel.Content = "Please Enter a number.";
                   ErrorLabel.Visibility = Visibility.Visible;
               }
            }
            else
            {
                ErrorLabel.Content = "Please fill in all fields";
                ErrorLabel.Visibility = Visibility.Visible;
            }
        }

        private void RecordRequestCrop(int amount)
        {

        }
        private void RecordRequestFertiliser(int amount)
        {
            FertiliserViewModel fertiliserVM = new FertiliserViewModel();

            fertiliserVM.RequestFertiliser(TypeComboBox.Text, amount);
        }
        private void RecordRequestContainer(int amount)
        {

        }
    }
}
