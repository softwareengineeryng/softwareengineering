﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPACCOTUTTO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void updateView()
        {
            

            // create the connection to the DB and open it
           
            usersDataGrid.ItemsSource = DatabaseConnection._instance().getDataSetAsList(Constants.SelectAll);
            //set up the data grid view
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            updateView();

/*            SPACCOTUTTO.Database1DataSet database1DataSet = ((SPACCOTUTTO.Database1DataSet)(this.FindResource("database1DataSet")));
            // Load data into the table Users. You can modify this code as needed.
            SPACCOTUTTO.Database1DataSetTableAdapters.UsersTableAdapter database1DataSetUsersTableAdapter = new SPACCOTUTTO.Database1DataSetTableAdapters.UsersTableAdapter();
            database1DataSetUsersTableAdapter.Fill(database1DataSet.Users);
            System.Windows.Data.CollectionViewSource usersViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("usersViewSource")));
            usersViewSource.View.MoveCurrentToFirst();
            */
        }

        private void usersDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}